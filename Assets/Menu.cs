﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void OnGUI()
    {
        GUI.contentColor = Color.white;
        if (GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 + 50, 100, 40), "START GAME"))
        {
            Application.LoadLevel(1);
        }
        if (GUI.Button(new Rect(Screen.width / 2 - 75, Screen.height / 2 + 100, 100, 40), "QUIT"))
        {
            Application.Quit();
        }
    }
}
