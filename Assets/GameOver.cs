﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void OnGUI () {
        GUI.contentColor = Color.yellow;
        if (GUI.Button(new Rect(Screen.width / 2 - 25, Screen.height / 2 + 100, 100, 40), "Retry?"))
        {
            Application.LoadLevel(1);
        }
        if (GUI.Button(new Rect(Screen.width / 2 - 25, Screen.height / 2 + 150, 100, 40), "Quit?"))
        {
            Application.Quit();
        }
	}
}
