﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterPhysics))]
public class CharacterMotion : MonoBehaviour {

	public float Speed = 8;
	public float Acceleration = 12;

	private float currentSpeed;
	private float targetSpeed;
	private float horizontalSpeed;
	private Vector2 amountToMove;

	private CharacterPhysics characterPhysics;

	private Animator hmAnimator;

	// Use this for initialization
	void Start() 
	{
		hmAnimator = GetComponent<Animator>();
		characterPhysics = GetComponent<CharacterPhysics>();
	}
	
	void Update() 
	{
		targetSpeed = Input.GetAxisRaw ("Horizontal") * Speed;
		horizontalSpeed = Input.GetAxis ("Horizontal");
		currentSpeed = IncrementTowards (currentSpeed, targetSpeed, Acceleration);
		hmAnimator.SetFloat ("Speed", Mathf.Abs(horizontalSpeed));


		if (Input.GetKey (KeyCode.D)) {
			transform.eulerAngles = new Vector2 (0, 0);
			amountToMove = new Vector2(currentSpeed, 0);
		}
		if (Input.GetKey (KeyCode.A)) {
			
			transform.eulerAngles = new Vector2 (0, 180);
			amountToMove = new Vector2(-currentSpeed, 0);
		}
		if (Input.GetKey (KeyCode.W)) {
			transform.Translate (Vector2.up * 8f * Time.deltaTime);
			
		}
		characterPhysics.Move (amountToMove * Time.deltaTime);
	}
	
	private float IncrementTowards(float c, float t, float a){
		if(c == t) 
		{
			return c;
		}
		else
		{
			float dir = Mathf.Sign(t - c);
			c += a * Time.deltaTime * dir;
			return (dir == Mathf.Sign (t-c))? c: t;
		}
	}
}