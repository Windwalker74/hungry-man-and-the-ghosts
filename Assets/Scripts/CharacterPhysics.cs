﻿using UnityEngine;
using System.Collections;

public class CharacterPhysics : MonoBehaviour {

	public void Move(Vector2 moveAmount)
	{
		transform.Translate (moveAmount);
	}
}
